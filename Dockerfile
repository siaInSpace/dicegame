FROM rust:1.45 as builder

WORKDIR /usr/src/dicegame

COPY . .
RUN cargo build --release

FROM debian:buster-slim

COPY --from=builder /usr/src/dicegame/target/release/dice_game .

CMD ["./dice_game"]
