use crate::dice::{Rule, Rules, Die, Throw};

use rand::distributions::{Distribution, Uniform};

pub fn thorw_die(sides : Die, n_die : usize) -> Throw
{
    let mut rng = rand::thread_rng();
    let mut throw : Throw = Throw::new();
    let t : Vec<Die> = Uniform::new_inclusive(1, sides).sample_iter(&mut rng).take(n_die).collect();
    for n in t
    {
        *throw.entry(n).or_default() += 1;
    }
    throw
}

fn create_rules() -> Rules
{
    let mut rules : Vec<Rule> = Vec::new();

    rules.push(Rule::from([(1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1)], 1500)); // Full straight
    rules.push(Rule::from([(1, 1), (2, 1), (3, 1), (4, 1), (5, 1)], 500));          //Low straight
    rules.push(Rule::from([(2, 1), (3, 1), (4, 1), (5, 1), (6, 1)], 750));          //High straight
    
    //Ones are special 
    rules.push(Rule::from([(1, 1)], 100));
    rules.push(Rule::from([(1, 3)], 1000));
    rules.push(Rule::from([(1, 4)], 2000));
    rules.push(Rule::from([(1, 5)], 4000));
    rules.push(Rule::from([(1, 6)], 8000));

    rules.push(Rule::from([(5, 1)], 50));
    
    for i in 2..=6
    {
        let base_score = i * 100;
        rules.push(Rule::from([(i, 3)], base_score));
        rules.push(Rule::from([(i, 4)], base_score*2));
        rules.push(Rule::from([(i, 5)], base_score*4));
        rules.push(Rule::from([(i, 6)], base_score*8));
    }
    
    Rules::new(rules)
}


pub fn find_selection(throw : &Throw, rules : &Rules) -> (Throw, usize, Rules)
{
    let mut selection = Throw::new();
    let mut score : usize = 0;
    let mut rules_used : Vec<Rule> = Vec::new();
    let mut _throw = throw.clone();

    loop
    {
        let mathces = rules.check(&_throw);
        if mathces.is_empty() { break; }

        let best_match = mathces.iter().max().unwrap();

        for (num, count) in best_match.get_pattern()
        {
            match _throw.get_mut(&num)
            {
                Some(n) => 
                {
                    if &count > n {break;}
                    *n -= count;
                    *selection.entry(num).or_default() += count;

                },
                None => {break;},
            }
        }

        score += best_match.get_score();
        rules_used.push(best_match.clone())

    }

    (selection, score, Rules::new(rules_used))
}




pub fn do_turn()
{
    let _score : usize;

    let rules : Rules = create_rules();
    let throw = thorw_die(6, 6);

    println!("{:?}", throw);
    println!("{:?}", rules.check(&throw));
    println!("{:?}", find_selection(&throw, &rules))

}
