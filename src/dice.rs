use std::collections::HashMap;
use std::cmp::Ordering;
pub type Die = usize;
pub type Throw = HashMap<Die, usize>;

#[derive(Clone, PartialEq, Debug, Eq)]
pub struct Rule
{
    pattern : Throw,
    score : usize
}

#[derive(Debug)]
pub struct Rules
{
    rules : Vec<Rule>
}

impl Rule
{
    pub fn new(pattern : Throw, score : usize) -> Rule
    {
        Rule{pattern, score}
    }

    pub fn from<const N : usize>(pattern : [(Die, usize); N], score : usize) -> Rule
    {
        Rule::new(Throw::from(pattern), score)
    }

    pub fn check(&self, throw : &Throw) -> bool
    {
        for (num, count) in &self.pattern
        {
            match throw.get(num)
            {
                Some(n) => {if n < count {return false}},
                None => {if count != &0 {return false;}},
            }
        }

       true
    }

    pub fn get_score(&self) -> usize
    {
        self.score
    }

    pub fn get_pattern(&self) -> Throw
    {
        self.pattern.clone()
    }
}

impl Ord for Rule
{
    fn cmp(&self, other : &Self) -> Ordering
    {
        self.score.cmp(&other.score)
    }
}

impl PartialOrd for Rule
{
    fn partial_cmp(&self, other : &Self) -> Option<Ordering>
    {
        Some(self.cmp(&other))
    }
}

impl Rules
{
    pub fn new (rules : Vec<Rule>) -> Rules
    {
        Rules{rules}
    }

    pub fn check(&self, throw : &Throw) -> Vec<Rule>
    {
        let mut matched_rules : Vec<Rule> = Vec::new();
        if throw.is_empty()
        { 
            return matched_rules
        }

        for rule in &self.rules
        {
            if rule.check(throw)
            {
                matched_rules.push(rule.clone());
            }
        }

        matched_rules
    }

    pub fn get_rules(&self) -> Vec<Rule>
    {
        self.rules.clone()
    }
}


#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_rule_check() 
    {
        let r = Rule::new(Throw::from([(1, 3)]), 1000);
        assert!(!r.check(&Throw::from([(1, 1), (2, 1), (3, 1)])));
        assert!(!r.check(&Throw::from([(1, 2), (2, 1)])));
        assert!(r.check(&Throw::from([(1, 3), (2, 1)])));
        assert!(r.check(&Throw::from([(1, 3)])));
    }

    #[test]
    fn test_multiple_rules_check()
    {
        let rule1 = Rule::new(Throw::from([(1, 3)]), 1000);
        let rule2 = Rule::new(Throw::from([(2, 3)]), 200);

        let rules = Rules::new(vec!(rule1.clone(), rule2.clone()));

        let empty_rules : Vec<Rule> = Vec::new();
        assert_eq!(rules.check(&Throw::from([(1, 1), (2, 1), (3, 1)])), empty_rules);
        assert_eq!(rules.check(&Throw::from([(1, 3)])), vec!(rule1.clone()));
        assert_ne!(rules.check(&Throw::from([(1, 3)])), vec!(rule2.clone()));
        assert_eq!(rules.check(&Throw::from([(1, 3), (2, 3)])), vec!(rule1.clone(), rule2.clone()));
    }
}